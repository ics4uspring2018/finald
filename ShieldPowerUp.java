import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ShieldPowerUp here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShieldPowerUp extends Actor
{
    /**
     * The power up that keeps the player from dying for 1 second after it is hit by an enemy.
     */
    
    GreenfootSound sound = new GreenfootSound("powerupsound.wav");
    
    public ShieldPowerUp() {
        setImage("shieldPowerUp.png");
    }
    public void act() 
    {
        if (getY()<getWorld().getHeight()-Background.ROAD_HEIGHT)
            setLocation(getX(),getY()+2);
        if (isTouching(Roach.class)) {
            sound.play();
            Roach.shield = true;
            Background.shielded = false;
            getWorld().removeObject(this);
        }
    }    
}
