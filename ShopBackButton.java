import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ShopBackButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShopBackButton extends Actor
{
    boolean mouseOver = false;
    GreenfootSound click = new GreenfootSound("Click On-SoundBible.com-1697535117.mp3");
    
    public void act() 
    {
        if (!mouseOver && Greenfoot.mouseMoved(this)) {
            setImage("ShopBackButton2.png");
            mouseOver = true;
            //click.play();
        }
        if (mouseOver && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this)) {
            setImage("ShopBackButton1.png");
            mouseOver = false;
        }
        if (Greenfoot.mouseClicked(this)) {
            click.play();
            Greenfoot.setWorld(new Menu());
        }
        //System.out.println("back "+getX()+","+getY());
    }       
}
