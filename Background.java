import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.concurrent.ThreadLocalRandom;
/**
 * Write a description of class Background here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Background extends World
{

    public static final int ROAD_HEIGHT = 65; //bottom of shoe is this many units from bottom of screen at max
    public static final int ROAD_HEIGHT_FEET = 55;
    public static final int ROACH_HEIGHT = 70; //y value of the bug
    public static final int WALL_WIDTH = 50; // width of the borders
    public static final int SCORE_HEIGHT = 40; // location of the score
    public static final int SCORE_SPACE = 44; //space between each score digit
    public static final int LEG_SPACE = 30; //space between each leg
    public static final int BIRD_SPACE = 20; //space to spawn snakes
    
    public static int GAME_TIME;
    
    public static boolean candyFirst; //if candy is the first drop
    public static boolean scoreSwitch; //if score exists
    public static boolean highScoreSwitch; //if the high score exists
    public static boolean newHighScoreSwitch; //if there is a new high score
    public static boolean gameOverSwitch; //if the game is over
    
    public static boolean shielded = false; //if the bug is shielded
    
    private int dropPercent; //drop rate for leaves, candy, power ups
    private int legPercent; //spawn rate for legs
    private int birdPercent; //spawn rate for snakes
    private boolean enemySwitch; //switch between snake and leg phases
    
    Roach r = new Roach();
    private InvisibleWall[] walls = new InvisibleWall[2]; //the boundaries for the player
    
    public Background()
    {    
        super(511, 307, 1, false); 
        //borders for the bug to stay within
        walls[0] = new InvisibleWall(WALL_WIDTH,getHeight()); 
        walls[1] = new InvisibleWall(WALL_WIDTH,getHeight());
        addObject(walls[0],WALL_WIDTH/2,getHeight()/2);
        addObject(walls[1],getWidth()-WALL_WIDTH/2,getHeight()/2);
        
        setPaintOrder(
            MenuButton.class,RetryButton.class,HighScore.class,BestScore.class,
            points.class,Game_Over.class,Leg.class,Bird.class,Candy.class,Leaf.class,Roach.class);        
        
        GAME_TIME = 0;
        dropPercent = 55; //the beginning percent for a leaf,candy,or power up to drop - 55%
        legPercent = 50; //the beginning percent for a pair of legs to spawn = 50%
        birdPercent = 40; //the beginning percent for a snake to spawn - 45%
        
        //set booleans
        candyFirst = false; //if the first item drop is a candy
        scoreSwitch = false; //if the score is present
        highScoreSwitch = false; //if the high score text is present
        gameOverSwitch = false; //if the player dies
        newHighScoreSwitch = false; //if there is a new high score
        shielded = false; //if the player is shielded by a power up
        enemySwitch = false; //switches between the leg phase and the snake phase (snakes and legs should not spawn regularly at the same time)
        
        points.SCORE = 0;
        Leg.LEG_COUNT = 0;
        addObject(r,getWidth()/2,getHeight()-ROACH_HEIGHT); //spawn the player
        dropper(); //spawn the first leaf/candy
    }
    public void act() {
        if (points.SCORE>0) {
            GAME_TIME++;
            score();
            shield();
            if (GAME_TIME%120==0) dropPercent++; //the drop percent increases by 1% every 2 seconds
            if (GAME_TIME%45==0) dropperTimer(dropPercent); //every 45 ticks (60 ticks = 1 sec) there will be a dropPercent% for a leaf,candy,or power up to drop
            if (GAME_TIME%200==0) { //every 200 ticks, the percent for an enemy to spawn increases by 1%
                legPercent++;
                birdPercent++;
            }
            if (GAME_TIME%300==0) { //switch between the snake and leg phases randomly every 5 seconds (it would be too hard if the snakes and legs were always there at the same time)
                int rand = ThreadLocalRandom.current().nextInt(1,3); //gets a random number from 1 to 2
                if (rand==1) enemySwitch = false;
                if (rand==2) enemySwitch = true;
            }
            if (GAME_TIME%90==0) { //an enemy has a chance to be spawned every 1.5 seconds
                if (!enemySwitch) legTimer(legPercent);
                if (enemySwitch) birdTimer(birdPercent);
            }
        }
        gameOver(); //only activates when the player dies
    }
    
    public void jumpPowerUp() { //creates a jump power up
        JumpPowerUp jump = new JumpPowerUp();
        addObject(jump,getWidth()/2-70,getHeight()-ROACH_HEIGHT);
    }
    
    public void shieldPowerUp() { //creates a shield power up
        int xLocation = ThreadLocalRandom.current().nextInt(WALL_WIDTH,getWidth()-WALL_WIDTH+1);
        ShieldPowerUp shield = new ShieldPowerUp();
        addObject(shield,xLocation,getHeight()-ROACH_HEIGHT);
    }
    
    public void shield() { //the visual shield effect
        Shield bubble = new Shield(r);
        if (r.shield && !shielded) {
            addObject(bubble,r.getX(),r.getY());
            shielded = true;
        }
    }
    
    public void powerUp() { //creates a speed power up
        PowerUp powerup = new PowerUp();
        addObject(powerup,getWidth()/2 + 60,getHeight()-ROACH_HEIGHT);
    }
    
    public void newBird() { //create a new snake
        int leftRandomizer = ThreadLocalRandom.current().nextInt(1,3);
        boolean left = leftRandomizer==1; //decides which direction the snake goes in
        int speed = ThreadLocalRandom.current().nextInt(3,7); //decides the speed of the snake
        Bird bird = new Bird(left, speed);
        if (left) addObject(bird,getWidth()+bird.getImage().getWidth()+BIRD_SPACE,getHeight()-ROACH_HEIGHT);
        else if (!left) addObject(bird,-bird.getImage().getWidth()-BIRD_SPACE,getHeight()-ROACH_HEIGHT);
    }
    
    public void newLeg() { //create a new pair of legs
        int rightRandomizer = ThreadLocalRandom.current().nextInt(1,3); 
        boolean right = (rightRandomizer==1);//decides which direction the legs will go in
        int hSpeed = ThreadLocalRandom.current().nextInt(2,7); //decides the horizontal speed of the legs
        int legType = ThreadLocalRandom.current().nextInt(0,6); //decides what type of legs these are
        Leg leg = new Leg(legType,hSpeed,6,right); 
        Leg leg2 = new Leg(legType,hSpeed,6,right); 
        int yLocation = ThreadLocalRandom.current().nextInt(Leg.LEG_MINY,getHeight()-ROAD_HEIGHT_FEET-Leg.getLegHeight()/2+1); //start legs at a random height
        if (right) {
            if(Leg.LEG_COUNT<4) {
                addObject(leg,-leg.getImage().getWidth()-LEG_SPACE,Leg.LEG_MINY);
                addObject(leg2,-leg.getImage().getWidth()*2-LEG_SPACE,Leg.LEG_MINY-70);
            }
        }
        else {
            if(Leg.LEG_COUNT<4) {
                addObject(leg,getWidth()+leg.getImage().getWidth()+LEG_SPACE,Leg.LEG_MINY);
                addObject(leg2,getWidth()+leg.getImage().getWidth()*2+LEG_SPACE,Leg.LEG_MINY-70);
            }
        }
    }
    
    public void legTimer(int percent) { //spawner of legs
        int max = 6;
        if (points.SCORE>=50) max = 8;
        int rand = ThreadLocalRandom.current().nextInt(0,101);
        boolean check = rand<=percent;
        if (Leg.LEG_COUNT<max) { //max legs on screen
            newLeg();
        }
    }
    
    public void birdTimer(int percent) { //spawner of snakes
        int max = 2;
        if (points.SCORE>=50) max = 3;
        int rand = ThreadLocalRandom.current().nextInt(0,101);
        boolean check = rand<=percent;
        if (Bird.BIRD_COUNT<=max) { //max snakes on screen
            newBird();
        }
    }
    
    public void dropper() { //spawns leaves, candies, powerups
        int check = ThreadLocalRandom.current().nextInt(0,101);
        int xLocation = ThreadLocalRandom.current().nextInt(WALL_WIDTH,getWidth()-WALL_WIDTH+1); //decides the x position of the drops
        int powerUp = ThreadLocalRandom.current().nextInt(1,4);
        Leaf leaf = new Leaf();
        Candy candy = new Candy();
        PowerUp speed = new PowerUp();
        JumpPowerUp jump = new JumpPowerUp();
        ShieldPowerUp shield = new ShieldPowerUp();
        if (check<=11) { //10% chance for candy to spawn
            addObject(candy,xLocation,-50);
        }
        else if (check<=16 && points.SCORE>0){ //5% chance for powerup to spawn, there can only be one of eache type on the screen at once
            if (powerUp==1 && getObjects(PowerUp.class).size()==0)addObject(speed,xLocation,-50);
            else if (powerUp==2 && getObjects(JumpPowerUp.class).size()==0) addObject(jump,xLocation,-50);
            else if (powerUp==3 && getObjects(ShieldPowerUp.class).size()==0)addObject(shield,xLocation,-50);
        }
        else //85% chance for leaf to spawn
            addObject(leaf,xLocation,-50);
    }
    
    public void dropperTimer(int percent) {//times the drops
        if (Roach.isDead) return;
        int rand = ThreadLocalRandom.current().nextInt(0,101);
        boolean check = (rand<=percent);
        if (check) dropper();
    }
    
    public void newLeaf() {
        Leaf leaf = new Leaf();
    }
    
    public void newCandy() {
        Candy candy = new Candy();
    }
    
    public void score() { //score display
        points ones = new points(false,false);
        points tens = new points(true,false);
        points hundreds = new points(false,true);
        if (!scoreSwitch && points.SCORE<10 || candyFirst) {
            scoreSwitch = true;
            addObject(ones,getWidth()/2+SCORE_SPACE/2,SCORE_HEIGHT);
            candyFirst = false;
            }
        if (scoreSwitch && points.SCORE>9 && points.SCORE<100) {
            addObject(tens,getWidth()/2-SCORE_SPACE/2,SCORE_HEIGHT);
            scoreSwitch = false;
        }
        if (!scoreSwitch && points.SCORE>99 && points.SCORE<1000) {
            addObject(hundreds,getWidth()/2-SCORE_SPACE/2-SCORE_SPACE,SCORE_HEIGHT);
            scoreSwitch = true;
        }
    }
    
    public void gameOver() {
        if (!Roach.isDead) return;
        if (!gameOverSwitch) {
            gameOverSwitch = true;
            //Retry Button:
            RetryButton retry = new RetryButton();
            addObject(retry,414,261);
            //Menu Button:
            MenuButton menu = new MenuButton();
            addObject(menu,98,265);
            //Text Banner
            Game_Over banner = new Game_Over();
            addObject(banner,getWidth()/2,getHeight()/2);
        }
        //BestScore       
        HighScore ones = new HighScore(false,false);
        HighScore tens = new HighScore(true,false);
        HighScore hundreds = new HighScore(false,true);
        HighScore highScore = new HighScore(false,false);
        BestScore bestScore = new BestScore();
        NewHighScore newHighScore = new NewHighScore();
        if (HighScore.highScore < points.SCORE && !newHighScoreSwitch) {
            newHighScoreSwitch = true;
            addObject(highScore,getWidth()/2,getHeight()-SCORE_HEIGHT*2);
        }
        if (HighScore.highScore<10 && !highScoreSwitch && !newHighScoreSwitch) {
            addObject(bestScore,255,215);
            highScoreSwitch = true;
            addObject(ones,getWidth()/2,getHeight()-SCORE_HEIGHT);
        }
        else if (HighScore.highScore<100 && !highScoreSwitch && !newHighScoreSwitch) {
            highScoreSwitch = true;
            addObject(bestScore,255,215);
            addObject(ones,getWidth()/2+SCORE_SPACE/2,getHeight()-SCORE_HEIGHT);
            addObject(tens,getWidth()/2-SCORE_SPACE/2,getHeight()-SCORE_HEIGHT);
        }
        else if (HighScore.highScore<1000 && !highScoreSwitch && !newHighScoreSwitch) {
            highScoreSwitch = true;
            addObject(bestScore,255,215);
            addObject(ones,getWidth()/2+SCORE_SPACE,getHeight()-SCORE_HEIGHT);
            addObject(tens,getWidth()/2,getHeight()-SCORE_HEIGHT);
            addObject(hundreds,getWidth()/2-SCORE_SPACE,getHeight()-SCORE_HEIGHT);
        }
    }
}
