import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.concurrent.ThreadLocalRandom;
/**
 * Write a description of class Game_Over here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Game_Over extends Actor
{    
    GreenfootImage[] text = {
        new GreenfootImage("GameOverBanner1.png"),
        new GreenfootImage("GameOverBanner2.png"),
        new GreenfootImage("GameOverBanner3.png"),
        new GreenfootImage("GameOverBanner4.png"),
        new GreenfootImage("GameOverBanner5.png")};
    public Game_Over() {
        int rand = ThreadLocalRandom.current().nextInt(0,5);
        setImage(text[rand]);
    }
}
