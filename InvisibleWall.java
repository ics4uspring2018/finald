import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class InvisibleWall here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class InvisibleWall extends Actor
{
    public InvisibleWall(int w, int h){
        GreenfootImage img = new GreenfootImage(w,h);
        setImage(img);
    }
}
