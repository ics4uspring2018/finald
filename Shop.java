import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Shop here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Shop extends World
{
    public static int AccPoints = 0; //beginning number of accumulated points, total can be no larger than 999
    boolean AccPointsSwitch;
    boolean costumesSwitch;
    
    public static boolean capBought = false; //if the cap skin is bought
    public static boolean cowboyBought = false; //if the cowboy skin is bought
    public static boolean clownBought = false; // if the clown skin is bought
    public static boolean antBought = false; //if the ant skin is bought
    
    /**
     * Constructor for objects of class Shop.
     * 
     */
    public Shop()
    {    
        super(511, 307, 1); 
        
        AccPointsSwitch = false; 
        costumesSwitch = false;
        
        costumes();
        backButton();
        unequipButton();
    }
    public void act() {
        AccPoints();
    }
    public void backButton() {
        ShopBackButton back = new ShopBackButton();
        addObject(back, 66, 280);
    }
    public void unequipButton() {
        ShopUnequipButton unequip = new ShopUnequipButton();
        addObject(unequip, 430, 280);
    }
    public void costumes() {
        ShopAntButton ant = new ShopAntButton();
        ShopClownButton clown = new ShopClownButton();
        ShopCowboyButton cb = new ShopCowboyButton();
        ShopCapButton cap = new ShopCapButton();
        
        addObject(cap,230,106);
        addObject(cb,410,105);
        addObject(clown,235,207);
        addObject(ant,405,205);
        costumesSwitch = true;
    }
    public void AccPoints() {
        AccPoints ones = new AccPoints(false,false);
        AccPoints tens = new AccPoints(true,false);
        AccPoints hundreds = new AccPoints(false,true);
        if (!AccPointsSwitch) {
            addObject(ones,114,150);
            addObject(tens,68,150);
            addObject(hundreds,22,150);
            AccPointsSwitch = true;
        }
    }
}
