import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ShopAntButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShopAntButton extends Actor
{
    /**
     * the button in the shop that allows the player to buy the 'ant' skin
     */
    
    boolean mouseOver = false;

    public static boolean equiped = false;
    GreenfootSound click = new GreenfootSound("Click On-SoundBible.com-1697535117.mp3");
    
    public ShopAntButton() {
        if (Shop.antBought) {
            if (equiped) setImage("ShopAntBought2.png");
            else setImage("ShopAntBought1.png");
        }
    }
    public void act() 
    {
        if (!equiped) {
            if (!mouseOver && Greenfoot.mouseMoved(this)) {
                if (Shop.antBought) setImage("ShopAntBought2.png");
                else setImage("ShopAntButton2.png");
                mouseOver = true;
            }
            if (mouseOver && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this)) {
                if (Shop.antBought) setImage("ShopAntBought1.png");
                else setImage("ShopAntButton1.png");
                mouseOver = false;
            }
            Roach.ant = false;
        }
        if (Greenfoot.mouseClicked(this)) {
            if (Shop.antBought) {
                equiped = !equiped;
            }
            if (Shop.AccPoints>=500 && !Shop.antBought){
                Shop.antBought = true;
                Shop.AccPoints-=500;
                equiped = true;
            }
            click.play();
        }
        if (equiped) {
            Roach.ant = true;
            ShopCowboyButton.equiped = false;
            ShopClownButton.equiped = false;
            ShopCapButton.equiped = false;
            ShopUnequipButton.clicked = false;
            
            if (Shop.antBought) setImage("ShopAntBought2.png");
            else setImage("ShopAntButton2.png");
        }
    } 
}
