import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MenuButtonPlay here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MenuButtonPlay extends Actor
{
    /**
     * The play button in the menu that leads to the main game.
     */
    
    boolean mouseOver = false;
    GreenfootSound click = new GreenfootSound("Click On-SoundBible.com-1697535117.mp3");
    
    public void act() 
    {
        if (!mouseOver && Greenfoot.mouseMoved(this)) {
            setImage("PlayButton2.png");
            mouseOver = true;
        }
        if (mouseOver && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this)) {
            setImage("PlayButton1.png");
            mouseOver = false;
        }
        if (Greenfoot.mouseClicked(this)) {
            click.play();
            Greenfoot.setWorld(new Background());
        }
    }    
}
