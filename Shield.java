import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Shield extends Actor
{
    private Roach r;
    int atime = 0;
    
    public Shield(Roach r) {
        setImage("Shield.png");
        this.r = r;
        atime = 0;
    }
    
    public void act() 
    {
        if (!Roach.shield) getWorld().removeObject(this);
        if (Roach.brokenShield) {
            if (atime==4) atime=0;
            if (atime==0) getImage().setTransparency(0);
            if (atime==2) getImage().setTransparency(255);
            atime++;
        }
        setLocation(r.getX(),r.getY());
    }    
}
