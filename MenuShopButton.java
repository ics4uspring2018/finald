import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MenuShopButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MenuShopButton extends Actor
{
    /**
     * The button that leads to the cosmetic shop, from the menu.
     */
    
    GreenfootSound click = new GreenfootSound("Click On-SoundBible.com-1697535117.mp3");
    boolean mouseOver = false;
    
    public void act() 
    {
        if (!mouseOver && Greenfoot.mouseMoved(this)) {
            setImage("ShopButton2.png");
            mouseOver = true;
        }
        if (mouseOver && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this)) {
            setImage("ShopButton1.png");
            mouseOver = false;
        }
        if (Greenfoot.mouseClicked(this)) {
            click.play();
            Greenfoot.setWorld(new Shop());
        }
    }   
}
