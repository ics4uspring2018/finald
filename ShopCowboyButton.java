import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ShopCowboyButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShopCowboyButton extends Actor
{
    boolean mouseOver = false;

    public static boolean equiped = false;
    GreenfootSound click = new GreenfootSound("Click On-SoundBible.com-1697535117.mp3");
    
    public ShopCowboyButton() {
        if (Shop.cowboyBought) {
            if (equiped) setImage("ShopCowboyBought2.png");
            else setImage("ShopCowboyBought1.png");
        }
    }
    public void act() 
    {
        if (!equiped) {
            if (!mouseOver && Greenfoot.mouseMoved(this)) {
                if (Shop.cowboyBought) setImage("ShopCowboyBought2.png");
                else setImage("ShopCowboyButton2.png");
                mouseOver = true;
            }
            if (mouseOver && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this)) {
                if (Shop.cowboyBought) setImage("ShopCowboyBought1.png");
                else setImage("ShopCowboyButton1.png");
                mouseOver = false;
            }
            Roach.cowboy = false;
        }
        if (Greenfoot.mouseClicked(this)) {
            if (Shop.cowboyBought) {
                equiped = !equiped;
            }
            if (Shop.AccPoints>=200 && !Shop.cowboyBought){
                Shop.cowboyBought = true;
                Shop.AccPoints-=200;
                equiped = true;
            }
            click.play();
        }
        if (equiped) {
            Roach.cowboy = true;
            ShopCapButton.equiped = false;
            ShopClownButton.equiped = false;
            ShopAntButton.equiped = false;
            ShopUnequipButton.clicked = false;
            if (Shop.cowboyBought) setImage("ShopCowboyBought2.png");
            else setImage("ShopCowboyButton2.png");
        }
    }    
}
