import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class AccPoints here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class AccPoints extends Actor
{
    /**
     * The accumulated number of points the player has. This is used to purchase skins.
     */
    
    GreenfootImage[] digits1 = {
        new GreenfootImage("Zero1.png"),
        new GreenfootImage("One1.png"),
        new GreenfootImage("Two1.png"),
        new GreenfootImage("Three1.png"),
        new GreenfootImage("Four1.png"),
        new GreenfootImage("Five1.png"),
        new GreenfootImage("Six1.png"),
        new GreenfootImage("Seven1.png"),
        new GreenfootImage("Eight1.png"),
        new GreenfootImage("Nine1.png")};
    GreenfootImage[] digits2 = {
        new GreenfootImage("Zero2.png"),
        new GreenfootImage("One2.png"),
        new GreenfootImage("Two2.png"),
        new GreenfootImage("Three2.png"),
        new GreenfootImage("Four2.png"),
        new GreenfootImage("Five2.png"),
        new GreenfootImage("Six2.png"),
        new GreenfootImage("Seven2.png"),
        new GreenfootImage("Eight2.png"),
        new GreenfootImage("Nine2.png")};
        
    public static int SCORE;
    int atime = 0;
    int oldSCORE = SCORE;
    private boolean tens; 
    private boolean hundreds;
    public AccPoints(boolean tens, boolean hundreds) {       
        this.tens = tens;
        this.hundreds = hundreds;
    }
    public void act() 
    {
        if (!tens && !hundreds) {
            if (atime==0) setImage(digits1[Shop.AccPoints%10]);
            atime++;
            if (atime==20)  setImage(digits2[Shop.AccPoints%10]);
            if (atime==40) atime=0;
        }
        if (tens && !hundreds) {
            if (atime==0) setImage(digits1[Shop.AccPoints/10%10]);
            atime++;
            if (atime==20)  setImage(digits2[Shop.AccPoints/10%10]);
            if (atime==40) atime=0;
        }
        if (!tens && hundreds) {
            if (atime==0) setImage(digits1[Shop.AccPoints/100]);
            atime++;
            if (atime==20)  setImage(digits2[Shop.AccPoints/100]);
            if (atime==40) atime=0;
        }
    }    
}
