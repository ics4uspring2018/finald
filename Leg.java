import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Leg here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Leg extends Actor
{
    /**
     * The main enemy in this game. The legs come in pairs, and their speed, and size vary randomly.
     * There is a limit to two pairs of legs on the screen at one time.
     */
    
    static final int LEG_SPACE = 30;
    
    int type;
    int stepSpeed;
    int upSpeed;
    boolean right;
    static int X;
    static int width;
    static int height;
    public static int LEG_COUNT = 0; //the number of legs on the screen
    public static int LEG_MINY = -70;
    public static int LEG_DELETE = 175; //pixels to give room for the leg to delete; prevents bugginess
    boolean legSwitch = true;
    int[] stepVar = new int[4];
    GreenfootImage[] legTypeLeft = {
        new GreenfootImage("LegLeft_90.png"),
        new GreenfootImage("LegLeft_90_2.png"),
        new GreenfootImage("LegLeft_70.png"),
        new GreenfootImage("LegLeft_70_2.png"),
        new GreenfootImage("LegLeft_50.png"),
        new GreenfootImage("LegLeft_50_2.png")};
    GreenfootImage[] legTypeRight = {
        new GreenfootImage("LegRight_90.png"),
        new GreenfootImage("LegRight_90_2.png"),
        new GreenfootImage("LegRight_70.png"),
        new GreenfootImage("LegRight_70_2.png"),
        new GreenfootImage("LegRight_50.png"),
        new GreenfootImage("LegRight_50_2.png")};
        
    GreenfootSound step = new GreenfootSound("horror-creature-footstep-grass-12.mp3");
    
    public Leg(int type, int stepSpeed, int upSpeed, boolean right) {
        this.type = type;
        this.stepSpeed = stepSpeed;
        this.upSpeed = upSpeed;
        this.right = right;
        this.width = getImage().getWidth();
        this.height = getImage().getHeight();
        if (right) setImage(legTypeRight[this.type]);
        if (!right) setImage(legTypeLeft[this.type]);
    }
    
    public int getType(){        
        return type;
    }
    public int getHSpeed() {
        return stepSpeed;
    }
    public int getVSpeed() {
        return upSpeed;
    }
    public boolean goingRight() {
        return right;
    }
    public static int getLegWidth() {
        return width;
    }
    public static int getLegHeight() {
        return height;
    }
    public static int getXPosition() {
        return X;
    }

    public void act() 
    {
        this.X=getX();
        if (legSwitch) {
            LEG_COUNT++;
            legSwitch=false;
        }
        if (getY()<=LEG_MINY) {
            if (upSpeed < 0) upSpeed = -upSpeed;
        }
        if (getY()>=getWorld().getHeight()-Background.ROAD_HEIGHT_FEET-getImage().getHeight()/2) {
            if (upSpeed > 0) upSpeed = -upSpeed;
            if (getX()>=0 && getX()<=getWorld().getWidth()) step.play();
        }
        if (right) {
            setLocation(getX()+stepSpeed,getY()+upSpeed);
        }
        if (!right) {
            setLocation(getX()-stepSpeed,getY()+upSpeed);
        }
        if (getX()>getWorld().getWidth()+getImage().getWidth()*2+LEG_SPACE+LEG_DELETE || getX()<-getImage().getWidth()*2-LEG_SPACE-LEG_DELETE) {
            LEG_COUNT--;
            getWorld().removeObject(this);
        }
    }    
}
