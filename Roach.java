import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


/**
 * Write a description of class Roach here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Roach extends Actor
{
    /**
     * The player. The player is a generic bug that can move left, right, and jump.
     * It's goal is to avoid feet and snakes while eating leaves and candy to gain points.
     * The power up interactions with the player are in this class.
     * The cosmetic skins are also in this class.
     */
    
    int atime=0; //the animation ticker
    int jumpTime = 0; //the amount of time the player will hover in the air after it reached it max jump height
    boolean used; //if the speed power up has been used
    public static int moveSpeed = 6; //generic movement speed
    public static boolean isDead; //if the player is dead
    public static boolean cap = false; //if the player has the cap skin on
    public static boolean cowboy =  false; //if the player has the cowboy skin on
    public static boolean clown = false; //if the player has the clown skin on
    public static boolean ant = false; //if the player has the ant skin on
    public static boolean shield = false; //if the player currently has the shield power up
    public static boolean brokenShield = false; //if the player has a shield, but was hit by an enemy within the past second
    public static boolean jumpBoost = false; //if the player currently has a jump boost;
    public static int jumpHeight = 307/2; //the generic jump height
    int stime = 0; //the timer for the shield to run out after the player hits an enemy
    int jtime = 0; //the timer for the jump boost to run out
    int ptime = 0; //the timer for the speed boost to run out
    public boolean inAir = false; //if the player is currently in the air
    boolean peak = false; //if the player has already hit the peak of its jump
    GreenfootSound squish = new GreenfootSound("Squish 1-SoundBible.com-662226724.mp3");
    
    public Roach() {
        isDead = false;
        inAir = false;
        peak = false;
        used = false;
        shield = false;
        jumpBoost = false;
        moveSpeed = 6;
        jumpHeight = 307/2;
        if (!cap && !cowboy && !clown && !ant) setImage("RoachRight1-1.png");
        if (cap) setImage("RoachRight2-1.png");
        if (cowboy) setImage("RoachRight3-1.png");
        if (clown) setImage("RoachRight4-1.png");
        if (ant) setImage("RoachRight5-1.png");
    }
    public void act() 
    {
        if (isDead) {
            if (getY()!=getWorld().getHeight()-Background.ROACH_HEIGHT) setLocation(getX(),getY()+8);
            else
            return;
        }
        int oldX = getX();
        if (!isDead) {
            if (!cap && !cowboy && !clown && !ant) { //if there is no skin equipped
                if (Greenfoot.isKeyDown("right")){ //right movement and animations
                    if(atime==0) setImage("RoachRight1-1.png");
                    atime++;
                    if(atime==4) setImage("RoachRight1-2.png");
                    if(atime==6) atime=0;
                    move(moveSpeed);
                }
                if (Greenfoot.isKeyDown("left")) { //left movement and animations
                    if(atime==0) setImage("Roach1-1.png");
                    atime++;
                    if(atime==4) setImage("Roach1-2.png");
                    if(atime==6) atime=0;
                    move(-moveSpeed);
                }
                if (Greenfoot.isKeyDown("up")) { //jump
                    inAir = true;
                }
                jump();
                if (isTouching(Leg.class) || isTouching(Bird.class)) { //if it is hit by an enemy
                    if (shield) stime++; 
                    else {
                        setImage("Roach1-3.png");
                        squish.play();
                        isDead = true;
                    }
                }   
                if (stime>0) {
                    brokenShield = true;
                    stime++;
                    if (stime>=60) {
                        stime = 0;
                        shield = false;
                        brokenShield = false;
                    }
                }
                if (isTouching(InvisibleWall.class)) { //keeps the player within the boundaries
                    setLocation(oldX,getY());
                }
            }
            
            if (cap) { //if the cap skin is used
                if (Greenfoot.isKeyDown("right")){
                    if(atime==0) setImage("RoachRight2-1.png");
                    atime++;
                    if(atime==4) setImage("RoachRight2-2.png");
                    if(atime==6) atime=0;
                    move(moveSpeed);
                }
                if (Greenfoot.isKeyDown("left")) {
                    if(atime==0) setImage("Roach2-1.png");
                    atime++;
                    if(atime==4) setImage("Roach2-2.png");
                    if(atime==6) atime=0;
                    move(-moveSpeed);
                }
                if (Greenfoot.isKeyDown("up")) {
                    inAir = true;
                }
                jump();
                if (isTouching(Leg.class) || isTouching(Bird.class)) {
                    if (shield) stime++;
                    else {
                        setImage("Roach2-3.png");
                        squish.play();
                        isDead = true;
                    }
                }   
                if (stime>0) {
                    stime++;
                    brokenShield = true;
                    if (stime>=60) {
                        stime = 0;
                        shield = false;
                        brokenShield = false;
                    }
                }   
                if (isTouching(InvisibleWall.class)) {
                    setLocation(oldX,getY());
                }
            }
            
            if (cowboy) { //if the cowboy skin is used
                if (Greenfoot.isKeyDown("right")){
                    if(atime==0) setImage("RoachRight3-1.png");
                    atime++;
                    if(atime==4) setImage("RoachRight3-2.png");
                    if(atime==6) atime=0;
                    move(moveSpeed);
                }
                if (Greenfoot.isKeyDown("left")) {
                    if(atime==0) setImage("Roach3-1.png");
                    atime++;
                    if(atime==4) setImage("Roach3-2.png");
                    if(atime==6) atime=0;
                    move(-moveSpeed);
                }
                if (Greenfoot.isKeyDown("up")) {
                    inAir = true;
                }
                jump();
                if (isTouching(Leg.class) || isTouching(Bird.class)) {
                    if (shield) stime++;
                    else {
                        setImage("Roach3-3.png");
                        squish.play();
                        isDead = true;
                    }
                }   
                if (stime>0) {
                    stime++;
                    brokenShield = true;
                    if (stime>=60) {
                        stime = 0;
                        shield = false;
                        brokenShield = false;
                    }
                }   
                if (isTouching(InvisibleWall.class)) {
                    setLocation(oldX,getY());
                }
            }
            
            if (clown) { //if the clown skin is used
                if (Greenfoot.isKeyDown("right")){
                    if(atime==0) setImage("RoachRight4-1.png");
                    atime++;
                    if(atime==4) setImage("RoachRight4-2.png");
                    if(atime==6) atime=0;
                    move(moveSpeed);
                }
                if (Greenfoot.isKeyDown("left")) {
                    if(atime==0) setImage("Roach4-1.png");
                    atime++;
                    if(atime==4) setImage("Roach4-2.png");
                    if(atime==6) atime=0;
                    move(-moveSpeed);
                }
                if (Greenfoot.isKeyDown("up")) {
                    inAir = true;
                }
                jump();
                if (isTouching(Leg.class) || isTouching(Bird.class)) {
                    if (shield) stime++;
                    else {
                        setImage("Roach4-3.png");
                        squish.play();
                        isDead = true;
                    }
                }   
                if (stime>0) {
                    stime++;
                    brokenShield = true;
                    if (stime>=60) {
                        stime = 0;
                        shield = false;
                        brokenShield = false;
                    }
                }    
                if (isTouching(InvisibleWall.class)) {
                    setLocation(oldX,getY());
                }
            }   
            
            if (ant) { //if the ant skin is used
                if (Greenfoot.isKeyDown("right")){
                    if(atime==0) setImage("RoachRight5-1.png");
                    atime++;
                    if(atime==4) setImage("RoachRight5-2.png");
                    if(atime==6) atime=0;
                    move(moveSpeed);
                }
                if (Greenfoot.isKeyDown("left")) {
                    if(atime==0) setImage("Roach5-1.png");
                    atime++;
                    if(atime==4) setImage("Roach5-2.png");
                    if(atime==6) atime=0;
                    move(-moveSpeed);
                }
                if (Greenfoot.isKeyDown("up")) {
                    inAir = true;
                }
                jump();
                if (isTouching(Leg.class) || isTouching(Bird.class)) {
                    if (shield) stime++;
                    else {
                        setImage("Roach5-3.png");
                        squish.play();
                        isDead = true;
                    }
                }   
                if (stime>0) {
                    stime++;
                    brokenShield = true;
                    if (stime>=60) {
                        stime = 0;
                        shield = false;
                        brokenShield = false;
                    }
                }    
                if (isTouching(InvisibleWall.class)) {
                    setLocation(oldX,getY());
                }
            }
            if(isTouching(PowerUp.class)){
                used = true;
            }
            powerUp();
            jumpBoost();
        }
    }
    
    public void jump() { //the method for jumping
        if (inAir) {
            if (!peak)
                setLocation(getX(),getY()-8);
            if (peak && jumpTime >= 10)
                setLocation(getX(),getY()+8);
            if (getY()<=jumpHeight) 
                peak = true;
            if (peak)
                jumpTime++;
            if (getY()>getWorld().getHeight()-Background.ROACH_HEIGHT) {
                inAir = false;
                peak = false;
                jumpTime = 0;
                setLocation(getX(),getWorld().getHeight()-Background.ROACH_HEIGHT);
            }
        }
    }
    
    public void powerUp(){ //if speed power up timer
        if(!Roach.isDead && used){
            ptime++;
            if(ptime==300 && (moveSpeed > 6)){
                moveSpeed -=3;
                used = false;
            }
        }
    }
    
    public void jumpBoost() { //jump power up timer
        if (jumpBoost) {
            jtime++;
            if (jtime==300) {
                jumpHeight = getWorld().getHeight()/2;
                jumpBoost = false;
            }
        }
    }
}
