import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ShopCapButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShopCapButton extends Actor
{
    boolean mouseOver = false;
    
    public static boolean equiped = false;
    GreenfootSound click = new GreenfootSound("Click On-SoundBible.com-1697535117.mp3");
    
    public ShopCapButton() {
        if (Shop.capBought) {
            if (equiped) setImage("ShopCapBought2.png");
            else setImage("ShopCapBought1.png");
        }
    }
    public void act() 
    {
        if (!equiped) {
            if (!mouseOver && Greenfoot.mouseMoved(this)) {
                if (Shop.capBought) setImage("ShopCapBought2.png");
                else setImage("ShopCapButton2.png");
                mouseOver = true;
            }
            if (mouseOver && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this)) {
                if (Shop.capBought) setImage("ShopCapBought1.png");
                else setImage("ShopCapButton1.png");
                
                mouseOver = false;
            }
            Roach.cap = false;
        }
        if (Greenfoot.mouseClicked(this)) {
            if (Shop.capBought) {
                equiped = !equiped;
            }
            if (Shop.AccPoints>=100 && !Shop.capBought){
                Shop.capBought = true;
                Shop.AccPoints-=100;
                equiped = true;
            }
            click.play();
        }
        if (equiped) {
            Roach.cap = true;
            ShopCowboyButton.equiped = false;
            ShopClownButton.equiped = false;
            ShopAntButton.equiped = false;
            ShopUnequipButton.clicked = false;
            
            if (Shop.capBought) setImage("ShopCapBought2.png");
            else setImage("ShopCapButton2.png");
        }
    }    
}
