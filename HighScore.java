import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class HighScore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class HighScore extends Actor
{
    public static int highScore;
    public static boolean HIGH_SCORE;
    
    GreenfootImage[] digits1 = {
        new GreenfootImage("Zero1.png"),
        new GreenfootImage("One1.png"),
        new GreenfootImage("Two1.png"),
        new GreenfootImage("Three1.png"),
        new GreenfootImage("Four1.png"),
        new GreenfootImage("Five1.png"),
        new GreenfootImage("Six1.png"),
        new GreenfootImage("Seven1.png"),
        new GreenfootImage("Eight1.png"),
        new GreenfootImage("Nine1.png")};
    GreenfootImage[] digits2 = {
        new GreenfootImage("Zero2.png"),
        new GreenfootImage("One2.png"),
        new GreenfootImage("Two2.png"),
        new GreenfootImage("Three2.png"),
        new GreenfootImage("Four2.png"),
        new GreenfootImage("Five2.png"),
        new GreenfootImage("Six2.png"),
        new GreenfootImage("Seven2.png"),
        new GreenfootImage("Eight2.png"),
        new GreenfootImage("Nine2.png")};
    
    int atime = 0;
    private boolean tens; 
    private boolean hundreds;
    boolean total;
    public static int newScore;
    boolean replaySwitch = false;
    boolean addToAccPoints = false;
    
    public HighScore(boolean tens, boolean hundreds) {
        this.tens = tens;
        this.hundreds = hundreds;
        HIGH_SCORE = false;
        total = false;
        newScore = points.SCORE;
    }
    public void act() 
    {        
        if (newScore > highScore && Background.newHighScoreSwitch || replaySwitch) {
            highScore = newScore;
            HIGH_SCORE = true;
            if (atime==0)setImage("NewHighScore1.png");
            atime++;
            if (atime==15) setImage("NewHighScore2.png");
            if (atime==30) atime = 0;  
            replaySwitch = true;
            if (!addToAccPoints) {
                Shop.AccPoints += points.SCORE;
                addToAccPoints = true;
            }
        }
        if (!tens && !hundreds && !Background.newHighScoreSwitch) {
            if (atime==0) setImage(digits1[highScore%10]);
            atime++;
            if (atime==20)  setImage(digits2[highScore%10]);
            if (atime==40) atime=0;
            if (!addToAccPoints) {
                Shop.AccPoints += points.SCORE;
                addToAccPoints = true;
            }
        }
        if (tens && !hundreds && !Background.newHighScoreSwitch) {
            if (atime==0) setImage(digits1[highScore/10%10]);
            atime++;
            if (atime==20)  setImage(digits2[highScore/10%10]);
            if (atime==40) atime=0;
            if (!addToAccPoints) {
                Shop.AccPoints += points.SCORE;
                addToAccPoints = true;
            }
        }
        if (!tens && hundreds && !Background.newHighScoreSwitch) {
            if (atime==0) setImage(digits1[highScore/100]);
            atime++;
            if (atime==20)  setImage(digits2[highScore/100]);
            if (atime==40) atime=0;
            if (!addToAccPoints) {
                Shop.AccPoints += points.SCORE;
                addToAccPoints = true;
            }
        }
    }    
}
