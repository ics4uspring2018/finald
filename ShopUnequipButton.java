import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ShopUnequipButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShopUnequipButton extends Actor
{
    boolean mouseOver = false;
    public static boolean clicked = false;
    GreenfootSound click = new GreenfootSound("Click On-SoundBible.com-1697535117.mp3");
    
    public ShopUnequipButton() {
        if (!ShopClownButton.equiped && !ShopAntButton.equiped && !ShopAntButton.equiped && !ShopAntButton.equiped) 
            setImage("ShopButtonUnequip2.png");
        else setImage("ShopUnequipButton1.png");
    }
    public void act() 
    {
        if (!mouseOver && Greenfoot.mouseMoved(this) && !clicked) {
            setImage("ShopButtonUnequip2.png");
            mouseOver = true;
        }
        if (mouseOver && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this) && !clicked) {
            setImage("ShopUnequipButton1.png");
            mouseOver = false;
        }
        if (Greenfoot.mouseClicked(this)) {
            clicked = !clicked;
            if (clicked) {
                setImage("ShopButtonUnequip2.png");
                ShopCapButton.equiped = false;
                ShopClownButton.equiped = false;
                ShopCowboyButton.equiped = false;
                ShopAntButton.equiped = false;
            }
            if (!clicked) setImage("ShopUnequipButton1.png");
            click.play();
        }
    }    
}
