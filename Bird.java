import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Bird here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Bird extends Actor
{
    /**
     * Originally planned to be a bird enemy, later decided a snake would fit better.
     * That's why the class and every variable related to it is called 'bird', but the sprite is a snake (the comments will refer to this as a snake).
     */
    
    public static int BIRD_COUNT = 0; //the number of snakes on the screen
    boolean left; //decides the direction
    int speed; //decides the speed
    int atime = 0; //animation ticker
    
    public Bird (boolean left, int speed) {
        this.left = left;
        this.speed = speed;
        if (left) setImage("snake0-1.png");
        else setImage("snake0.png");
        BIRD_COUNT++;
    }
    
    public void act() 
    {
        if (left) {
            setLocation(getX()-speed,getY());
            if (atime==0) setImage("snake0-1.png");
            atime++;
            if (atime==4) setImage("snake1-1.png");
            if (atime==8) setImage("snake2-1.png");
            if (atime==12) atime = 0;
        }
        if (!left) {
            setLocation(getX()+speed,getY());
            if (atime==0) setImage("snake0.png");
            atime++;
            if (atime==4) setImage("snake1.png");
            if (atime==8) setImage("snake2.png");
            if (atime==12) atime = 0;
        }
        if (getX()>getWorld().getWidth()+getImage().getWidth()*2+Leg.LEG_SPACE+Leg.LEG_DELETE || getX()<-getImage().getWidth()*2-Leg.LEG_SPACE-Leg.LEG_DELETE) {
            BIRD_COUNT--;
            getWorld().removeObject(this);
        }
    }    
}
