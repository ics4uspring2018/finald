import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class InstructionsButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class InstructionsButton extends Actor
{
    /**
     * A button that leads from the menu to the instructions page.
     */
    
    boolean mouseOver = false;
    GreenfootSound click = new GreenfootSound("Click On-SoundBible.com-1697535117.mp3");
    public void InstructionsButton() {
        setImage("instructions.png");
    }
    
    public void act() 
    {
        if (!mouseOver && Greenfoot.mouseMoved(this)) {
            setImage("instructions2.png");
            mouseOver = true;
        }
        if (mouseOver && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this)) {
            setImage("instructions.png");
            mouseOver = false;
        }
        if (Greenfoot.mouseClicked(this)) {
            click.play();
            Greenfoot.setWorld(new Instructions());
        }
    }    
}
