import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Menu here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Menu extends World
{
    /**
     * .The Main Menu of this game.
     * 
     */
    GreenfootSound theme = new GreenfootSound("06_Theme of PuyoPuyo.mp3");
    public Menu()
    {    
        super(511, 307, 1); 
        playButton();
        shopButton();
        instructionsButton();
    }
    public void act() {
        if (!musicCheck.music) {
            theme.playLoop();
            musicCheck.music = true;
        }      
    }
    public void playButton() {
        MenuButtonPlay play = new MenuButtonPlay();
        addObject(play,getWidth()/2,getHeight()/2);
    }
    public void shopButton() {
        MenuShopButton shop = new MenuShopButton();
        addObject(shop,getWidth()/2,getHeight()-getHeight()/4);
    }
    public void instructionsButton() {
        InstructionsButton instructions = new InstructionsButton(); 
        addObject(instructions,483,280);
    }
}
