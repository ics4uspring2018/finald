import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class BestScore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class BestScore extends Actor
{
    /**
     * the text that appears in the game over screen, which lets the player know what the current high score is.
     */
    
    public BestScore() {
        setImage("BestScore.png");
    }
}
