import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class JumpPowerUp here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class JumpPowerUp extends Actor
{
    /**
     * a jump power up that increases the player's jump height for 5 seconds.
     */
    
    GreenfootSound sound = new GreenfootSound("powerupsound.wav");
    
    public JumpPowerUp() {
        setImage("jumpPowerUp.png");
    }
    public void act() 
    {
        if (getY()<getWorld().getHeight()-Background.ROAD_HEIGHT)
            setLocation(getX(),getY()+2);
        if (isTouching(Roach.class)) {
            sound.play();
            Roach.jumpHeight = getWorld().getHeight()/4;
            Roach.jumpBoost = true;
            getWorld().removeObject(this);
        }
    }    
}
