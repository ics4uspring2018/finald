import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ShopClownButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShopClownButton extends Actor
{
    /**
     * The button in the shop that allows the player to buy a clown suit.
     */
    boolean mouseOver = false;

    public static boolean equiped = false;     
    GreenfootSound click = new GreenfootSound("Click On-SoundBible.com-1697535117.mp3");
    
    public ShopClownButton() {
        if (Shop.clownBought) {
            if (equiped) setImage("ShopClownBought2.png");
            else setImage("ShopClownBought1.png");
        }
    }
    public void act() 
    {
        if (!equiped) {
            if (!mouseOver && Greenfoot.mouseMoved(this)) {
                if (Shop.clownBought) setImage("ShopClownBought2.png");
                else setImage("ShopClownButton2.png");
                mouseOver = true;
            }
            if (mouseOver && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this)) {
                if (Shop.clownBought) setImage("ShopClownBought1.png");
                else setImage("ShopClownButton1.png");
                mouseOver = false;
            }
            Roach.clown = false;
        }
        if (Greenfoot.mouseClicked(this)) {
            if (Shop.clownBought) {
                equiped = !equiped;
            }
            if (Shop.AccPoints>=300 && !Shop.clownBought){
                Shop.clownBought = true;
                Shop.AccPoints-=300;
                equiped = true;
            }
            click.play();
        }
        if (equiped) {
            Roach.clown = true;
            ShopCowboyButton.equiped = false;
            ShopCapButton.equiped = false;
            ShopAntButton.equiped = false;
            ShopUnequipButton.clicked = false;
            
            if (Shop.clownBought) setImage("ShopClownBought2.png");
            else setImage("ShopClownButton2.png");
        }
    }    
}
