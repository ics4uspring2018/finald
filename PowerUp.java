import greenfoot.*;
public class PowerUp extends Actor
{
    /**
     * a Speed power up that increases the players speed for 5 seconds
     */
    
    GreenfootSound sound = new GreenfootSound("powerupsound.wav");
    public PowerUp() {
        setImage("powerup.png");
    }
    public void act(){
        if (getY()<getWorld().getHeight()-Background.ROAD_HEIGHT)
            setLocation(getX(),getY()+2);
        if(isTouching(Roach.class) && !Roach.isDead) {
            sound.play();
            Roach.moveSpeed += 3;
            sound.play();
            getWorld().removeObject(this);
        }
    }  
}