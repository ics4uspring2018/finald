import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Leaf here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Leaf extends Actor
{
    /**
     * An object that the player can eat. It gives one point, and is the most common drop that spawns.
     */
    
    int atime=0;
    GreenfootSound bite = new GreenfootSound("single_fast_bite_of_potato_chip.mp3");
    public void act() 
    {
        if (getY()<getWorld().getHeight()-Background.ROAD_HEIGHT)
            setLocation(getX(),getY()+2);
        if(isTouching(Roach.class) && !Roach.isDead) {
            atime++;
            if(atime==12) {
                bite.play();
                setImage("Leaf2.png");
            }
            if(atime==24) {
                setImage("Leaf3.png");
                bite.play();
            }
            if(atime==36) {
                setImage("Leaf4.png");
                bite.play();
            }
            if(atime==48) {
                setImage("Leaf5.png");
                bite.play();
            }
            if(atime==60) {
                bite.play();
                points.SCORE++;
                getWorld().removeObject(this);
            }
        }
    }    
}
