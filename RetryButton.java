import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class RetryButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RetryButton extends Actor
{
    boolean mouseOver = false;
    GreenfootSound click = new GreenfootSound("Click On-SoundBible.com-1697535117.mp3");
    
    public void act() 
    {
        if (!mouseOver && Greenfoot.mouseMoved(this)) {
            setLocation(getX(),getY()+25);
            mouseOver = true;
        }
        if (mouseOver && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this)) {
            setLocation(getX(),getY()-25);
            mouseOver = false;
        }
        if (Greenfoot.mouseClicked(this)){
            click.play();
            Greenfoot.setWorld(new Background());
        }
    }    
}
