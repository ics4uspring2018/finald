import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Candy here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Candy extends Actor
{
    /**
     * An object that the player can eat. It gives 10 points, but takes longer to be eaten.
     */
    
    int atime=0;
    GreenfootSound bite = new GreenfootSound("single_fast_bite_of_potato_chip.mp3");
    public void act() 
    {
        if (getY()<getWorld().getHeight()-Background.ROAD_HEIGHT)
            setLocation(getX(),getY()+2);
        if(isTouching(Roach.class) && !Roach.isDead) {
            atime++;
            if(atime==12) {
                setImage("Wrapper2.png");
                bite.play();
            }
            if(atime==24) {
                setImage("Wrapper3.png");
                bite.play();
            }
            if(atime==36) {
                setImage("Wrapper4.png");
                bite.play();
            }
            if(atime==48) {
                setImage("Candy1.png");
                bite.play();
            }
            if(atime==60) {
                setImage("Candy2.png");
                bite.play();
            }
            if(atime==72) {
                setImage("Candy3.png");
                bite.play();
            }
            if(atime==84) {
                setImage("Candy4.png");
                bite.play();
            }
            if(atime==96) {
                setImage("Candy5.png");
                bite.play();
            }
            if(atime==108) {
                if (points.SCORE==0){
                    Background.scoreSwitch = true;
                    Background.candyFirst = true;
                }
                bite.play();
                points.SCORE+=10;
                getWorld().removeObject(this);
            }
        }
    }    
}
